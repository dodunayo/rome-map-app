import React, { Component } from 'react';
import { Map, GoogleApiWrapper } from 'google-maps-react';
//import logo from './logo.svg';
/*import './App.css'; */


const mapStyles = {
	width: '100%',
	height: '100%'
};

export class MapContainer extends Component {
	render() {
		return (
			<Map
				google={this.props.google}
				zoom={14}
				style={mapStyles}
				initialCenter={{
					lat: -1.2884,
					lng: 36.8233
				}}
			/>
		);
	}
}

export default GoogleApiWrapper({
	apiKey: 'AIzaSyAP_pkFBKs4Thep64i7kmckDZlknRJmNM0'
})(MapContainer);
